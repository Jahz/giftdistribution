How to write a rule file:

1) On the first line, add all names participating to the "gift chain", separated by a blank space
2) Then add as many lines as you want, and for each line the program will make sure that the first person will NOT offer to all subsequent names

And that's all, then just change the fileName in GiftDistribution.java, and run it, the console output will give you the distribution :) 